# sails-hook-sequelize

## Exemple de configuration

```js
sequelize: {
    models: 'api/sequelize',
    connection: {
        uri: process.env.DATABASE_URL,
        options: {
            timezone: 'Europe/Paris',
        },
    },
},
```