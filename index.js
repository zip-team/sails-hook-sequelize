const fs = require('fs');
const path = require('path');
const process = require('process');
const Sequelize = require('sequelize');
const DataTypes = require('sequelize').DataTypes;

const cwd = process.cwd();

module.exports = function sequelizeHook(sails) {
    return {
        defaults: {

        },

        configure() {
            const config = sails.config[this.configKey];
            if (!config.connection.uri) throw new Error(`No "${this.configKey}.connection.uri" configuration variable provided.`);
        },

        initialize(next) {
            const config = sails.config[this.configKey];

            if (!config) {
                sails.warn(`No ${this.configKey} found.`);
                return next();
            }

            sails.db = {}; // eslint-disable-line no-param-reassign

            const sequelize = new Sequelize(config.connection.uri, config.connection.options);

            const files = fs
                .readdirSync(path.join(cwd, config.models))
                .filter(file => (file.indexOf('.') !== 0) && (file.slice(-3) === '.js'));

            files.forEach((file) => {
                const schema = require(path.join(cwd, config.models, file)); // eslint-disable-line global-require, import/no-dynamic-require
                if (schema.definition) {
                    const model = schema.definition(sequelize, DataTypes);
                    sails.db[model.name] = model; // eslint-disable-line no-param-reassign
                }
            });

            files.forEach((file) => {
                const schema = require(path.join(cwd, config.models, file)); // eslint-disable-line global-require, import/no-dynamic-require
                if (schema.associations) {
                    schema.associations(sails.db);
                }
            });

            sails.db.sequelize = sequelize; // eslint-disable-line no-param-reassign
            next();
        },
    };
};
