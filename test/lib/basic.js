/* global describe, before, after, it */

const Sails = require('sails').Sails;

function test(sequelizeHook) {
    describe('Basic test :: Complete hook lifecycle', () => {
        // const to hold a running sails app instance
        let sails;

        // Before running any tests, attempt to lift Sails
        before(function before(done) {
            // Hook will timeout in 10 seconds
            this.timeout(11000);

            // Attempt to lift sails
            Sails().lift({
                hooks: {
                    // Load the hook
                    sequelize: sequelizeHook,
                    // Skip grunt (unless your hook uses it)
                    grunt: false,
                    orm: false,
                    pubsub: false,
                },
                log: {
                    level: 'error',
                },
                sequelize: {
                    models: 'test/models',
                    connection: {
                        uri: process.env.DATABASE_URL,
                        options: {
                            timezone: 'Europe/Paris',
                        },
                    },
                },
            }, (err, _sails) => {
                if (err) return done(err);
                sails = _sails;
                return done();
            });
        });

        // After tests are complete, lower Sails
        after((done) => {
            // Lower Sails (if it successfully lifted)
            if (sails) {
                return sails.lower(done);
            }
            // Otherwise just return
            return done();
        });

        // Test that Sails can lift with the hook in place
        it('Sails does not crash', () => true);
    });
}

module.exports = test;
