/* global describe */

const basicTest = require('./lib/basic');
const sequelizeHook = require('../');

describe('Local (hook from local source code)', () => {
    basicTest(sequelizeHook);
});
