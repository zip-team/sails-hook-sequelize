/* global describe */

const basicTest = require('./lib/basic');
const sequelizeHook = require('sails-hook-sequelize');

describe('Remote (hook from remote git repository)', () => {
    basicTest(sequelizeHook);
});
